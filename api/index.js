const express = require('express');
const cors = require('cors');
const mongoose = require("mongoose");
const { nanoid } = require('nanoid');
const User = require("./models/User");
const Message = require("./models/Message");
const users = require('./app/users');
const config = require('./config');
const app = express();

const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use('/users', users);



require('express-ws')(app);
const activeConnection = {};

app.ws('/chat', async (ws, req) => {
    const id = nanoid();
    const token = req.query.token;
    console.log('client connected id=', id);

    if (token === 'undefined') {
        console.log('No token!');
        return;
    }

    const user = await User.findOne({token});
    user.status = true;
    await user.save();

    const users = await User.find();

    if (!user) {
        console.log('No User!');
        return;
    }

    let decodedMessage = {};
    let savedMessages = [];
    let username = user.displayName;

    activeConnection[id] = ws;

    Object.keys(activeConnection).forEach(id => {
        const conn = activeConnection[id];
        conn.send(JSON.stringify({
            type: 'USER',
            user: username,
            users: users,
        }));
    });

    savedMessages = await Message.find().sort({ _id: -1 }).limit(30);

    ws.send(JSON.stringify({
        type: 'PREV_MESSAGES',
        messages: savedMessages,
    }))


    ws.on('message', async (msg) => {
        decodedMessage = JSON.parse(msg);

        const messageData = {
            username: username,
            text: decodedMessage.text,
        };
        const message = new Message(messageData);
        await message.save();

        switch (decodedMessage.type) {
            case 'SEND_MESSAGE':
                Object.keys(activeConnection).forEach(id => {
                    const conn = activeConnection[id];
                    conn.send(JSON.stringify({
                        type: 'NEW_MESSAGE',
                        message: {
                            username,
                            text: decodedMessage.text
                        }
                    }));
                });
                break
            default:
                console.log('Unknown message: ', decodedMessage.type);
        }
    });


    ws.on('close', async () => {
        console.log('client disconnected id=', id);
        delete activeConnection[id];

        user.status = false;
        await user.save();
        const users = await User.find();
        Object.keys(activeConnection).forEach(id => {
            const conn = activeConnection[id];
            console.log('activeConnection close');
            conn.send(JSON.stringify({
                type: 'USER',
                user: username,
                users: users,
            }));
        });
    });
});

const run = async () => {
    await mongoose.connect(config.mongo.db, config.mongo.options);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    process.on('exit', () => {
        mongoose.disconnect();
    });
};

run().catch(e => console.error(e));