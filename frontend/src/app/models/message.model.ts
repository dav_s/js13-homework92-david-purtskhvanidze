import { User } from './user.model';

export interface Message {
  username: string,
  text: string
}

export interface ServerMessage {
  type: string,
  user: string,
  users: User[],
  message: Message,
  messages: Message[],
}
