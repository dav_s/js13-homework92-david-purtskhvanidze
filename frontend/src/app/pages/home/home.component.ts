import { Component, OnDestroy, OnInit } from '@angular/core';
import { Message, ServerMessage } from '../../models/message.model';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { User } from '../../models/user.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit, OnDestroy{
  user: Observable<null | User>;
  token: string | undefined;

  username = '';
  messageText = '';
  messages: Message[] = [];
  users: User[] = [];
  ws!: WebSocket;

  constructor(
    private http: HttpClient,
    private store: Store<AppState>
  ) {
    this.user = store.select(state => state.users.user);
    this.user.pipe(map((user) => user!.token)).subscribe((v) => this.token = v);
  }


  ngOnInit() {
    this.ws = new WebSocket(`ws://localhost:8000/chat?token=${this.token}`);
    this.ws.onclose = () => console.log("ws closed");

    this.ws.onmessage = event => {
      const decodedMessage: ServerMessage = JSON.parse(event.data);

      if (decodedMessage.type === 'NEW_MESSAGE') {
        this.messages.push(decodedMessage.message);
      }

      if (decodedMessage.type === 'PREV_MESSAGES') {
        decodedMessage.messages.forEach(m => {
          this.messages.unshift(m)
        })
      }

      if (decodedMessage.type === 'USER') {
        this.users = decodedMessage.users
      }
    };
  };

  sendMessage() {
    this.ws.send(JSON.stringify({
      type: 'SEND_MESSAGE',
      text: this.messageText
    }));

    this.messageText = '';
  }

  ngOnDestroy() {
    this.ws.close();
  };
}
